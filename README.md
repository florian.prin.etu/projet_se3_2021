# Tutorat de Programmation Avancée (SE3 - 2020/2021)

Ce dépôt `GIT` contient le projet de programmation avancée du semestre 6 de Ali KHALAF et Florian PRIN.

## Résumé

Le but de ce projet est de réaliser une application qui permet d'analyser 58592 vols aux États Unis en 2014 en langage C. Pour cela nous avons chargé des fichiers pour effectuer un certain nombre de requêtes (lister des vols selon plusieurs critères, lister l'aéroligne avec plus de retards, trouver un itinéraire, ...) qui seront expliqué précédemment.

## Contenu du dépôt GIT

Notre dépôt dispose de plusieurs répertoires situés à sa racine où nous allons retrouver tous les fichiers nécessaires à la réalisation de l'application :

* Tout d'abort à la __racine__ :
  * __.clang-format__ : Fichier permettant le formatage du code C (fichier de Mr. DEQUIDT)
  * __.gitignore__ : Ce fichier nous permet de ne pas voir les fichiers d'autosave lors des ajouts sur le dépôt git
  * __.Makefile__ : Fichier composé des paramètres de compilation
  * __README.md__ : Fichier sur lequel nous sommes actuellement
  * __Rapport.pdf__ : Compte-rendu du projet au format pdf


* Dans le répertoire __data__ :
  * __airlines.csv__ : Données des compagnies aériennes
  * __airports.csv__ : Données des aéroports
  * __flights.csv__ : Données des vols
  * __requetes.txt__ : Fichier contenant des requêtes de tests


* Dans le répertoire __include__ :
  * __data.h__ : Fichier contenant les entêtes des fonctions data
  * **load_data.h** : Fichier contenant les entêtes des fonctions load data
  * __requetes.h__ : Fichier contenant les entêtes des fonctions requêtes


* Dans le répertoire __src__ :
  * **load_data.c** : Contenant toutes les fonctions utilisables pour les listes chainées et table de hachage + les fonctions utilitaires
  * __main.c__ : Gestion des fichier csv + appel aux fonctions et utilisation des requêtes pour l'affichage
  * __requetes.c__ : Contient toutes les implémentations des requêtes ainsi que les fonctions intermédiaires utilisées pour les requêtes.

## Le fichier `Makefile`
  
Un fichier `Makefile` générique inspiré de celui de M.DEQUIDT, celui-ci compile avec `gcc` et prend en paramètre pour sa compilation `-g -W -Wall -Wextra -MMD -O0` ce qui va nous permettre de compiler automatiquement notre projet de pouvoir aussi générer les fichiers.o des fichiers.c et de créer un seul exécutable pour l'ensemble du projet.
  
Pour utiliser notre programme une fois notre __dépot GIT__ récupérer, il va donc falloir utiliser la commande `make` dans le terminal à la racine de notre projet afin d'exécuter le makefile et de générer les fichiers.o et la compilation avec gcc.

## Etapes à réaliser pour l'utilisation de notre programme

1. Une fois le dépôt __GIT cloner__, l'utilisateur devra utiliser la commande `make`à la racine du dépôt afin de compiler tous les fichiers du projet pour le fonctionnement de l'application, puis une fois la commande faite un fichier `project` va être créé ce sera donc l'exécutable de tout le projet. 
2. Avant l'exécution du programme avec `./project` l'utilisateur va devoir renseigner sur le fichier `requetes.txt` les requêtes qu'il souhaitera utiliser selon les requetes déjà implémentées et expliquées à la suite du __README__. ou alors il aura une seconde possibilité sans modifier le fichier `requetes.txt` il pourra alors exécuter directement le programme.
3. Alors un message de bienvenue sera affiché et demande à l'utilisateur de saisir le chiffre, si l'utilisateur choisit le chiffre 1 alors l'exécution et traitement des données et des requêtes sera fait par le fichier `requetes.txt` normalement modifier et adapter par l'utilisateur. Ou s'il tape un chiffre différent de 1 alors l'utilisateur va pouvoir rentrer les requêtes souhaiter avec le bon format comme ci-dessous manuellement dans le terminal.
4. Après l'exécution d'une des 2 méthodes nous aurons un affichage défini pour chaques requetes dans le terminal qui respecte le cahier des charges.
5. Enfin une fois les informations obtenues l'utilisateur aura le choix soit de recontinuer à taper des requêtes s'il utilise la deuxième méthode ou alors il peut mettre fin à l'application en tapant q ou quit dans le terminal.

Une fois le programme fini on pourra le re exécuter en tapant dans le terminal `./project`.

## Utilisation des requêtes 

Notre programme fonctionne selon un cycle :

1. charger le fichier de données
2. attendre une commande
3. traiter la commande
4. afficher le résultat de cette commande
5. revenir à l'étape 2

Les commandes seront les suivantes:

- `show-airports <airline_id>`  : affiche tous les aéroports depuis lesquels la compagnie aérienne `<airline_id>` opère des vols
- `show-airlines <port_id>`: affiche les compagnies aériens qui ont des vols qui partent de l'aéroport `<port_id>`
- `show-flights <port_id> <date>` : affiche les vols qui partent de l'aéroport à la date, avec optionnellement une heure de début, et limité à xx vols __(les paramètres optionnels [<time>] [limit=<xx>] pour cette fonction n'ont pas été implémenté)__
- `most-delayed-flights`     : donne les 5 vols qui ont subis les plus longs retards à l'arrivée
- `most-delayed-airlines`    : donne les 5 compagnies aériennes qui ont, en moyenne, le plus de retards
- `delayed-airline <airline_id>`    : donne le retard moyen de la compagnie aérienne passée en paramètre
- `most-delayed-airlines-at-airport <airport_id>`    : donne les 3 compagnies aériennes avec le plus de retard d'arrivé à l'aéroport passée en paramètre __(Nous n'avons pas réussi à implémenter cette fonction)__
- `changed-flights <date>` : les vols annulés ou déviés à la date <date> (format M-D) 
- `avg-flight-duration <port_id> <port_id>`: calcule le temps de vol moyen entre deux aéroports
- `find-itinerary <port_id> <port_id> <date> <time> <limit>`: trouve un ou plusieurs itinéraires entre deux aéroports à une date donnée (l'heure est optionnel, requête peut être limité à `xx` propositions, il peut y avoir des escales) __(la requête fonctionne mais n'affiche que pour une escale, pour les paramètres time et limit pour notre requêtes ne sont pas optionnels)__
- `find-multicity-itinerary <port_id_depart> <port_id_dest1> <date> [<time>] <port_id_dest2> <date> [<time>] ... <port_id_destN> <date> [<time>]`: trouve un itinéraire multiville qui permet de visiter plusieurs villes (il peut y avoir des escales pour chaque vol intermediaire). __(Nous n'avons pas réussi à implémenter cette fonction)__
- `quit` ou `q` : Pour quitter le programme

Pour information, les paramètres entre crochets `[ ]` sont optionnels et les paramètres entre `< >` indiquent une valeur à renseigner.

Dans le cas d'une commande non trouvable le programme va redemander une pour la deuxième méthode où l'on rentre manuellement les requêtes.

